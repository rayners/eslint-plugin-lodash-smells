'use strict';

var rule = require('../lib/rules/no-each-push'),
    RuleTester = require('eslint').RuleTester;

var ruleTester = new RuleTester();
ruleTester.run('no-each-push', rule, {
    valid: [
        '_.each(myList, function(item) {\n' +
            '\tvar newItem = doSomething(item);\n' +
            '});',
        '_.forEach(myList, function(item) {\n' +
            '\tvar newItem = doSomething(item);\n' +
            '});'
    ],
    invalid: [
        {
            code: '_.each(myList, function(item) {\n' +
                '\tvar newItem = doSomething(item);\n' +
                '\tresults.push(newItem);\n' +
                '});',
            errors: 1
        },
        {
            code: '_.forEach(myList, function(item) {\n' +
                '\tvar newItem = doSomething(item);\n' +
                '\tresults.push(newItem);\n' +
                '});',
            errors: 1
        }
    ]
});
