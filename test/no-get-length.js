"use strict";

var rule = require('../lib/rules/no-get-length'),
    RuleTester = require('eslint').RuleTester;

var ruleTester = new RuleTester();
ruleTester.run('no-get-length', rule, {
    valid: [
        '_.get(aVar, "myLength")',
        '_.get(aVar, "myLength", 5)',
        '_.get([1,2,3], "myLength")',
        '_.get([1,2,3], "myLength", 5)',
        '_.size(aVar)'
    ],
    invalid: [
        {
            code: '_.get(aVar, "length")',
            errors: 1
        },
        {
            code: '_.get(aVar, "length", 0)',
            errors: 1
        },
        {
            code: '_.get([1,2,3], "length")',
            errors: 1
        },
        {
            code: '_.get([1,2,3], "length", 0)',
            errors: 1
        }
    ]
});
