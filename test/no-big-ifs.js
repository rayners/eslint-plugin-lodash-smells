"use strict";

var rule = require('../lib/rules/no-big-ifs'),
    RuleTester = require('eslint').RuleTester;

var ruleTester = new RuleTester();
ruleTester.run('no-big-fs', rule, {
    valid: [
        '_.each(myList, function(item) {\n' +
            '\tdoSomething(list);\n' +
            '\tdoAnotherThing(list);\n' +
            '});\n',
        '_.each(_.filter(myList, function(i) { return true; }), function(item) {\n' +
            '\tdoSomething(list);\n' +
            '\tdoAnotherThing(list);\n' +
            '});\n',
        '_.forEach(myList, function(item) {\n' +
            '\tdoSomething(list);\n' +
            '\tdoAnotherThing(list);\n' +
            '});\n',
        '_.forEach(_.filter(myList, function(i) { return true; }), function(item) {\n' +
            '\tdoSomething(item);\n' +
            '\tdoAnotherThing(item);\n' +
            '});\n',
        '_(myList).\n' +
            'filter(function(i) { return true; }).\n' +
            'each(function(item) {\n' +
            '\tdoSomething(item);\n' +
            '\tdoAnotherThing(item);\n' +
            '});\n'
    ],
    invalid: [
        {
            code: '_.each(myList, function(item) {\n' +
                '\tif (false) {\n' +
                '\t\tvar i = 0;\n' +
                '\t}\n' +
                '});\n',
            errors: 1
        },
        {
            code: '_.forEach(myList, function(item) {\n' +
                '\tif (false) {\n' +
                '\t\tvar i = 0;\n' +
                '\t}\n' +
                '});\n',
            errors: 1
        },
        {
            code: '_(myList).\n' +
                'each(function(item) {\n' +
                '\tif (false) {\n' +
                '\t\tvar i = 0;\n' +
                '\t}\n' +
                '});\n',
            errors: 1
        }

    ]
});
