ESLint-plugin-lodash-smells
===========================

[ ![Codeship Status for rayners/eslint-plugin-lodash-smells](https://img.shields.io/codeship/3e221720-2e94-0133-1da1-6a18900ed8b9/master.svg)](https://codeship.com/projects/99148)
[![Coverage Status](https://coveralls.io/repos/rayners/eslint-plugin-lodash-smells/badge.svg?branch=master&service=bitbucket)](https://coveralls.io/bitbucket/rayners/eslint-plugin-lodash-smells?branch=master)

ESLint rules for lodash uses I keep an eye out for when I am doing code reviews.

## Installation

Install [ESLint](http://esling.org) either locally or globally.

    $ npm install eslint

If you installed `ESLint` globally, you have to install the lodash smells plugin globally as well. Otherwise, install it locally.

    $ npm install eslint-plugin-lodash-smells

## Configuration

Add a `plugins` section to your `.eslintrc` (if it is not there already), and add `lodash-smells` to the list:


    {
        "plugins": [
            "lodash-smells"
        ]
    }

And then enable the rules you would like to use:

    {
        "rules": {
            "lodash-smells/no-big-ifs": 1,
            "lodash-smells/no-each-push": 1,
            "lodash-smells/no-get-length": 1
        }
    }

## Rules

### `no-big-ifs`

Intended to avoid code in this form:

    _.each(myList, function(item) {
        if (someCondition(item) {
            doSomething(item);
        }
    })


Prefered:

    _(myList)
        .filter(someCondition)
        .each(doSomething);
    
    _.each(_.filter(myList, someCondition), doSomething);


### `no-each-push`

Intended to avoid code in this form:

    var myNewList = []
    _.each(myList, function(item) {
        var newItem = doSomething(item);
        myNewList.push(newItem);
    });
    
Preferred:

    var myNewList = _.map(myList, doSomething);


### `no-get-length`

Intended to avoid code in this form:

    var l = _.get(myList, 'length', 0);

Preferred:

    var l = _.size(myList);
