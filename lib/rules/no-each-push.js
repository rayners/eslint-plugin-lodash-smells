'use strict';

var _ = require('lodash'),
    utils = require('../utils');

module.exports = function(context) {

    // Ugh, but it works
    function isArrayPush(object) {
        return object.type && object.type === 'ExpressionStatement' &&
            object.expression && object.expression.type === 'CallExpression' &&
            object.expression.callee && object.expression.callee.property &&
            object.expression.callee.property.type &&
            object.expression.callee.property.type === 'Identifier' &&
            object.expression.callee.property.name === 'push';
    }

    function noEachPush(node) {
        if (utils.isLodashCall(node, ['each', 'forEach'])) {
            var functionArgument = _.last(node.parent.arguments);
            if (functionArgument.body && functionArgument.body.body) {
                var finalStatement = _.last(functionArgument.body.body);
                if (isArrayPush(finalStatement)) {
                    context.report(node, "Each with a push is really just map");
                }
            }
        }
    }

    return {
        'MemberExpression': noEachPush
    };
};

module.exports.schema = [];
