"use strict";

var _ = require('lodash'),
    utils = require('../utils');

module.exports = function(context) {

    var funcs = [ 'each', 'forEach' ];

    function noBigIfs(node) {

        if (utils.isLodashCall(node, funcs)) {
            // it's a lodash each/forEach call

            var functionArgument = _.last(node.parent.arguments);
            if (functionArgument.type === 'FunctionExpression' && functionArgument.body &&
                functionArgument.body.body && _.size(functionArgument.body.body) === 1 &&
                functionArgument.body.body[0].type === 'IfStatement') {
                context.report(node, "An each call with a function with a giant if is iffy");
            }
        }
    }

    return {
        'MemberExpression': noBigIfs
    };
};

module.exports.schema = [];
