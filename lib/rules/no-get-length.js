"use strict";

var _ = require('lodash'),
    utils = require('../utils');

module.exports = function(context) {

    function noGetLength(node) {
        if (utils.isLodashCall(node, 'get')) {

            var memberArgument = _(node.parent.arguments).rest().first();

            if (_.isEqual(memberArgument.type, 'Literal') &&
                _.isEqual(memberArgument.value, 'length')) {
                context.report(node, "Prefer _.size to _.get(<whatever>, 'length')");
            }
        }
    }

    return {
        'MemberExpression': noGetLength
    };
};
