'use strict';

module.exports = {
    rules: {
        'no-big-ifs': require('./rules/no-big-ifs'),
        'no-each-push': require('./rules/no-each-push')
    },
    rulesConfig: {
        'no-big-ifs': 0,
        'no-each-push': 0
    }
};
