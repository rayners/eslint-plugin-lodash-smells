'use strict';

var _ = require('lodash');

function fromLodash(object) {
    return object && object.type &&
        ((object.type === 'Identifier' &&
          object.name === '_') ||
         (object.type === 'CallExpression' &&
          fromLodash(object.callee)));
}

function isLodashCall(node, methods) {
    return fromLodash(node.object) &&
        (
            (_.isArray(methods) &&
             _.includes(methods, node.property.name))
                ||
                (!_.isArray(methods) &&
                 _.isEqual(methods, node.property.name))
        );
}
module.exports = {
    fromLodash: fromLodash,
    isLodashCall: isLodashCall
};
